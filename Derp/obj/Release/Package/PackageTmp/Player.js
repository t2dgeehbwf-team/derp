var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
define(["require", "exports"], function (require, exports) {
    var Player = (function (_super) {
        __extends(Player, _super);
        function Player(x, y, width, height, engine, direction) {
            _super.call(this, x, y, width, height, ex.Color.Chartreuse);
            this.direction = direction;
            this.collisionType = 2 /* Active */;
        }
        Player.prototype.update = function (engine, delta) {
            _super.prototype.update.call(this, engine, delta);
            //x velocity in pixels/seconds
            var playerSpeed = 1;
            var maxspeed = 50;
            var minspeed = 1;
            var gravity = 20;
            var keyboardInUse = false;
            //GO LEFT
            this.dx = 0;
            this.dy = 0;
            if (engine.input.keyboard.isKeyPressed(37 /* Left */)) {
                this.dx = -maxspeed;
                console.log(this.x, this.y, this.dx);
            }
            //GO RIGHT
            if (engine.input.keyboard.isKeyPressed(39 /* Right */)) {
                this.dx = maxspeed;
                console.log(this.x, this.y, this.dx);
            }
            //GO UP
            if (engine.input.keyboard.isKeyPressed(38 /* Up */)) {
                this.dy = -maxspeed;
                console.log(this.x, this.y, this.dy);
            }
            //GO DOWN
            if (engine.input.keyboard.isKeyPressed(40 /* Down */)) {
                this.dy = maxspeed;
                console.log(this.x, this.y, this.dx);
            }
            //COLLISION
            // (left edge) x is less than 0 reset
            if (this.x < 0) {
                this.x = 0;
            }
            //player x velocity + playerwidth lesser than screenwidth
            if (this.x + this.getWidth() > engine.getWidth()) {
                this.x = engine.getWidth() - this.getWidth();
                this.dx = 0;
            }
            if (this.y < 0) {
                this.y = 0;
            }
            if (this.y + this.getHeight() > engine.getHeight()) {
                this.y = engine.getHeight() - this.getHeight();
                this.dy = 0;
            }
        };
        return Player;
    })(ex.Actor);
    return Player;
});
//class Player extends ex.Actor {
//    public initialize(game) {
//        this.player = new Player();
//        this.player.
//        game.addChild(this.player);
//    }
//    public update(game) {
//        if (game.input.keyboard.isKeyPressed(ex.Input.Keys.W) ||
//            game.input.keyboard.isKeyPressed(ex.Input.Keys.Up)) {
//            console.log("Going up!");
//        } else if (game.input.keyboard.isKeyPressed(ex.Input.Keys.S) ||
//            game.input.keyboard.isKeyPressed(ex.Input.Keys.Down)) {
//            console.log("Going down!");
//        } else if (game.input.keyboard.isKeyPressed(ex.Input.Keys.A) ||
//            game.input.keyboard.isKeyPressed(ex.Input.Keys.Left)) {
//            console.log("Going left!");
//        } else if (game.input.keyboard.isKeyPressed(ex.Input.Keys.D) ||
//            game.input.keyboard.isKeyPressed(ex.Input.Keys.Right)) {
//            console.log("Going right!");
//        }
//    }
//}
//export = Player; 
//# sourceMappingURL=player.js.map