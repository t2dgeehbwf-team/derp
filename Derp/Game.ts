﻿class Game extends ex.Engine {
    
    constructor() {
        super({ width: 800, height: 600, displayMode: ex.DisplayMode.FullScreen });
    }

    public start() {
        // add custom scenes
        this.add("mainmenu", new MainMenu());

        return super.start().then(() => {

            this.goToScene("mainmenu");

            // custom start-up
        });
    }

   
}

export = Game;

class MainMenu extends ex.Scene
{
    // start-up logic, called once
    public onInitialize(engine: ex.Engine) {
        engine.backgroundColor = ex.Color.Green;
    }

    // each time the scene is entered (Engine.goToScene)
    public onActivate() {
        
    }

    // each time the scene is exited (Engine.goToScene)
    public onDeactivate() {
        
    }
}
