/// <reference path="scripts/excalibur-0.5.1.d.ts" />
/// <reference path="config.ts" />
/// <reference path="resources.ts" />
/// <reference path="player.ts" />
define(["require", "exports", "Player", "Resources"], function (require, exports, Player, Resources) {
    var game = new ex.Engine(800, 600);
    // load assets
    var player = new Player(100, 100, 64, 64, game, 1);
    game.backgroundColor = ex.Color.Azure;
    var loader = new ex.Loader([
        Resources.IdleLeft,
        Resources.walkLeft
    ]);
    // start game
    game.start(loader);
    var playerSpriteSheet = new ex.SpriteSheet(Resources.IdleLeft, 7, 1, 64, 64);
    var playerIdleAnimation = playerSpriteSheet.getAnimationForAll(game, 125);
    playerIdleAnimation.loop = true;
    player.addDrawing("idle", playerIdleAnimation);
    game.addChild(player);
});
//# sourceMappingURL=app.js.map