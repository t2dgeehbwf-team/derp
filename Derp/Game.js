var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
define(["require", "exports"], function (require, exports) {
    var Game = (function (_super) {
        __extends(Game, _super);
        function Game() {
            _super.call(this, { width: 800, height: 600, displayMode: 0 /* FullScreen */ });
        }
        Game.prototype.start = function () {
            var _this = this;
            // add custom scenes
            this.add("mainmenu", new MainMenu());
            return _super.prototype.start.call(this).then(function () {
                _this.goToScene("mainmenu");
                // custom start-up
            });
        };
        return Game;
    })(ex.Engine);
    var MainMenu = (function (_super) {
        __extends(MainMenu, _super);
        function MainMenu() {
            _super.apply(this, arguments);
        }
        // start-up logic, called once
        MainMenu.prototype.onInitialize = function (engine) {
            engine.backgroundColor = ex.Color.Green;
        };
        // each time the scene is entered (Engine.goToScene)
        MainMenu.prototype.onActivate = function () {
        };
        // each time the scene is exited (Engine.goToScene)
        MainMenu.prototype.onDeactivate = function () {
        };
        return MainMenu;
    })(ex.Scene);
    return Game;
});
//# sourceMappingURL=Game.js.map