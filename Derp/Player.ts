﻿
class Player extends ex.Actor {
    constructor(x, y, width, height, engine: ex.Engine, public direction: number) {
        super(x, y, width, height, ex.Color.Chartreuse);

        this.collisionType = ex.CollisionType.Active;
        
     
    }
    
    public update(engine: ex.Engine, delta: number, player: Player) {
        super.update(engine, delta);
 
        var maxspeed = 50;
      
        //GO LEFT
        this.dx = 0;
        this.dy = 0;

        if (engine.input.keyboard.isKeyPressed(ex.Input.Keys.Left)) {
            this.dx = -maxspeed;
            console.log(this.x, this.y, this.dx);

            
        } 
        //GO RIGHT
        if (engine.input.keyboard.isKeyPressed(ex.Input.Keys.Right)) {
       
                this.dx = maxspeed;
                console.log(this.x , this.y , this.dx);
        } 
        //GO UP
        if (engine.input.keyboard.isKeyPressed(ex.Input.Keys.Up)) {
                this.dy = -maxspeed;
                console.log(this.x, this.y, this.dy);
        } 
        //GO DOWN
        if (engine.input.keyboard.isKeyPressed(ex.Input.Keys.Down)) {
            this.dy = maxspeed;
            console.log(this.x, this.y, this.dx);
        }
      
        //COLLISION
        // (left edge) x is less than 0 reset
        if (this.x < 0) {
            this.x = 0;
        }
        //player x velocity + playerwidth lesser than screenwidth
        if (this.x + this.getWidth() > engine.getWidth()) {
            this.x = engine.getWidth() - this.getWidth();
            this.dx = 0;
        }

      

        if (this.y < 0) {
            this.y = 0;
            
        }

        if (this.y + this.getHeight() > engine.getHeight()) {
            this.y = engine.getHeight() - this.getHeight();
            this.dy = 0;
        }

    }
    
}


export = Player;








//class Player extends ex.Actor {
  
//    public initialize(game) {
        
//        this.player = new Player();
//        this.player.
//        game.addChild(this.player);
//    }
//    public update(game) {
         

//        if (game.input.keyboard.isKeyPressed(ex.Input.Keys.W) ||
//            game.input.keyboard.isKeyPressed(ex.Input.Keys.Up)) {

//            console.log("Going up!");
//        } else if (game.input.keyboard.isKeyPressed(ex.Input.Keys.S) ||
//            game.input.keyboard.isKeyPressed(ex.Input.Keys.Down)) {
//            console.log("Going down!");
            
//        } else if (game.input.keyboard.isKeyPressed(ex.Input.Keys.A) ||
//            game.input.keyboard.isKeyPressed(ex.Input.Keys.Left)) {
//            console.log("Going left!");
            
//        } else if (game.input.keyboard.isKeyPressed(ex.Input.Keys.D) ||
//            game.input.keyboard.isKeyPressed(ex.Input.Keys.Right)) {
//            console.log("Going right!");
            
//        }
//    }
     
//}

//export = Player;